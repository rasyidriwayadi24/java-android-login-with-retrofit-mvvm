package com.example.loginapi;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ILoginService {

    @POST("/api/v1/auth/login/")
    Call<LoginResponse> login(@Body LoginBody loginBody);
}
