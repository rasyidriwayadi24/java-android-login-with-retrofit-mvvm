package com.example.loginapi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tvLogin;
    Button btn_l;
    EditText editUsername, editPassword;
    ProgressBar lProgressBar;

    MainViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvLogin = findViewById(R.id.tvLoginResult);
        editUsername = findViewById(R.id.edt_username);
        editPassword = findViewById(R.id.edt_password);
        lProgressBar = findViewById(R.id.progressBar);
        btn_l = findViewById(R.id.btn_login);

        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        mViewModel.getProgress().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer visibilyty) {
                lProgressBar.setVisibility(visibilyty);
            }
        });

        mViewModel.getLoginResult().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                tvLogin.setText(s);
            }
        });

        btn_l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.login(editUsername.getText().toString(), editPassword.getText().toString());
            }
        });

    }
}